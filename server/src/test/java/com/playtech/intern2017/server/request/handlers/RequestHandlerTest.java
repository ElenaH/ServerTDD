package com.playtech.intern2017.server.request.handlers;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.mockito.Mockito.*;

import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.playtech.intern2017.protocol.messages.query.Bet;
import com.playtech.intern2017.protocol.messages.ResponseBuilder;

import java.util.Iterator;

@RunWith(MockitoJUnitRunner.class)

public class RequestHandlerTest {
/*

@Test
public void validateBet_returnsUserIdInvalid(){
	spy(BetCollectionManager.class);
	when(BetCollectionManager.isUserIdValid(any(Bet.class))).thenReturn(false);
	RequestHandler.validateBet( any(Bet.class),any(ResponseBuilder.class) );
	when(BetCollectionManager.isBetIdValid(any(Bet.class))).thenReturn(false);
	
}*/
	 @Test
	 public void iterator_will_return_hello_world(){
	  //arrange
	  Iterator i=mock(Iterator.class);
	  when(i.next()).thenReturn("Hello").thenReturn("World");
	  //act
	  String result=i.next()+" "+i.next();
	  //assert
	  assertEquals("Hello World", result);
	 }
	
	 
/*
 * VERIFY THE METHOD WAS CALLED OR WAS NEVER CALLED
 * verify(accountManager).findAccount(customer)
Verify that accountManager.withdraw(account, amount) was never called.
1
verify(accountManager, times(0)).withdraw(account, withdrawlAmount2000);



 public class CustomerService {

    @Inject
    private CustomerDao customerDao;
    
 //
	    @Mock
	    private CustomerDao daoMock;

	    @InjectMocks
	    private CustomerService service;

	    @Before
	    public void setUp() throws Exception {

	         MockitoAnnotations.initMocks(this);
	    }

	    @Test
	    public void test() {

	         //assertion here
	    }
*/

/*
 private static Response handleQueryBetsRequest(QueryBetsRequest request) {
		QueryBetsResponseBuilder newResponseBuilder = new QueryBetsResponseBuilder(request);
		BetFilter newFilter = request.getFilter();
		newResponseBuilder.setBets(BetManager.getFilteredBets(newFilter));
		newResponseBuilder.setStatusCode(ResponseStatus.STATUS_SUCCESS);
		Response response = (Response) newResponseBuilder.build();
		return response;
	}
 */
	
	
}
