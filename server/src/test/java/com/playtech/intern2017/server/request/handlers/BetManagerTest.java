package com.playtech.intern2017.server.request.handlers;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.junit.*;
import org.mockito.Mock;
import org.mockito.Spy;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import static com.playtech.intern2017.protocol.data.BetStatus.*;
import com.playtech.intern2017.protocol.messages.query.Bet;
import com.playtech.intern2017.protocol.messages.query.BetFilter;
import com.playtech.intern2017.server.request.handlers.BetManager;

public class BetManagerTest {
	final static long START_TIMESTAMP = (long) 1488326400 * 1000;
	static long currentTimeStamp = new Timestamp(System.currentTimeMillis()).getTime();
	private BetFilter filter = new BetFilter(null, null, null, null, null, null, null);
	@Spy
	private BetFilter spyFilter = spy(filter);
	@Mock
	private BetFilter mockFilter = mock(BetFilter.class);
	@Mock
	private Bet mockBet = mock(Bet.class);
	private static Bet bet = new Bet("otherBet-0", "sfb", currentTimeStamp, 1, ACTIVE);
	public static BetManager testBetManager = new BetManager();

	@BeforeClass
	public static void test_isStartTimeStampCorrect() throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		format.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date date = format.parse("2017-03-01T00:00:00");
		long ts = new Timestamp(date.getTime()).getTime();
		assertEquals(ts, START_TIMESTAMP);
	}

	@BeforeClass
	public static void setUp() {
		testBetManager.addBet(bet);
	}

	@Test
	public void testIsBetIdValid_valid() {
		when(mockBet.getBetId()).thenReturn("svb-1");
		assertTrue(testBetManager.isBetIdValid(mockBet));
	}

	@Test
	public void testIsBetIdValid_invalidNull() {
		when(mockBet.getBetId()).thenReturn(null);
		assertFalse(testBetManager.isBetIdValid(mockBet));
	}

	@Test
	public void testIsBetIdValid_invalidEmpty() {
		when(mockBet.getBetId()).thenReturn("");
		assertFalse(testBetManager.isBetIdValid(mockBet));
	}

	@Test
	public void testIsBetIdValid_invalidSpaces() {
		when(mockBet.getBetId()).thenReturn("   ");
		assertFalse(testBetManager.isBetIdValid(mockBet));
	}

	@Test
	public void testIsUserIdValid_valid() {
		when(mockBet.getUserId()).thenReturn("svb");
		assertTrue(testBetManager.isUserIdValid(mockBet));
	}

	@Test
	public void testIsUserIdValid_invalidNull() {
		when(mockBet.getUserId()).thenReturn(null);
		assertFalse(testBetManager.isUserIdValid(mockBet));
	}

	@Test
	public void testIsUserIdValid_invalidEmpty() {
		when(mockBet.getUserId()).thenReturn("");
		assertFalse(testBetManager.isUserIdValid(mockBet));
	}

	@Test
	public void testIsUserIdValid_invalidSpaces() {
		when(mockBet.getUserId()).thenReturn("   ");
		assertFalse(testBetManager.isUserIdValid(mockBet));
	}

	@Test
	public void testisTimeStampAfterStart_validAfterStart() {
		when(mockBet.getTimeStamp()).thenReturn(START_TIMESTAMP + 1000);
		assertTrue(testBetManager.isTimeStampAfterStart(mockBet));
	}

	@Test
	public void testisTimeStampAfterStart_validStart() {
		when(mockBet.getTimeStamp()).thenReturn(START_TIMESTAMP);
		assertTrue(testBetManager.isTimeStampAfterStart(mockBet));
	}

	@Test
	public void testisTimeStampAfterStart_invalidEarly() {
		when(mockBet.getTimeStamp()).thenReturn(START_TIMESTAMP - 1000);
		assertFalse(testBetManager.isTimeStampAfterStart(mockBet));
	}

	@Test
	public void testIsTimeStampBeforeEnd_validBeforeEnd() {
		when(mockBet.getTimeStamp()).thenReturn(currentTimeStamp - 1000);
		assertTrue(testBetManager.isTimeStampBeforeEnd(mockBet));
	}

	@Test
	public void testIsTimeStampBeforeEnd_validEnd() {
		when(mockBet.getTimeStamp()).thenReturn(currentTimeStamp);
		assertTrue(testBetManager.isTimeStampBeforeEnd(mockBet));
	}

	@Test
	public void testIsTimeStampBeforeEnd_invalidLate() {
		when(mockBet.getTimeStamp()).thenReturn(currentTimeStamp + 1000);
		assertFalse(testBetManager.isTimeStampBeforeEnd(mockBet));
	}

	@Test
	public void testIsAmountNegative_positive() {
		when(mockBet.getAmount()).thenReturn(100L);
		assertFalse(testBetManager.isAmountNegative(mockBet));
	}

	@Test
	public void testIsAmountNegative_zero() {
		when(mockBet.getAmount()).thenReturn(0L);
		assertFalse(testBetManager.isAmountNegative(mockBet));
	}

	@Test
	public void testIsAmountNegative_negative() {
		when(mockBet.getAmount()).thenReturn(-100L);
		assertTrue(testBetManager.isAmountNegative(mockBet));
	}

	@Test
	public void testIsAlreadyCancelled_active() {
		when(mockBet.getStatus()).thenReturn(ACTIVE);
		assertFalse(testBetManager.isAlreadyCancelled(mockBet));
	}

	@Test
	public void testIsAlreadyCancelled_cancelled() {
		when(mockBet.getStatus()).thenReturn(CANCELLED);
		assertTrue(testBetManager.isAlreadyCancelled(mockBet));
	}

	@Test
	public void testDoesntExist_active() {
		when(mockBet.getStatus()).thenReturn(ACTIVE);
		assertFalse(testBetManager.doesntExist(mockBet));
	}

	@Test
	public void testDoesntExist_doesntExist() {
		when(mockBet.getStatus()).thenReturn(null);
		assertTrue(testBetManager.doesntExist(mockBet));
	}

	@Test
	public void testIsUnique_unique() {
		when(mockBet.getBetId()).thenReturn("svb-1");
		assertTrue(testBetManager.isUnique(mockBet));
	}

	@Test
	public void testIsUnique_nonunique() {
		when(mockBet.getBetId()).thenReturn(bet.getBetId());
		assertFalse(testBetManager.isUnique(mockBet));
	}

	@Test
	public void testbetMatchesFilter_doesntMatch() {
		filter = new BetFilter("not-regularBet-0", "not-rb", currentTimeStamp - 100, currentTimeStamp + 100, (long) 0,
				2L, ACTIVE);
		assertFalse(testBetManager.betMatchesFilter(bet, filter));
		filter = new BetFilter(null, null, null, null, null, null, null);
	}

	@Test
	public void testbetMatchesFilter_nullFilter() {
		filter = null;
		assertTrue(testBetManager.betMatchesFilter(bet, filter));
		filter = new BetFilter(null, null, null, null, null, null, null);
	}

	@Test
	public void testbetMatchesFilter_emptyFilter() {
		assertTrue(testBetManager.betMatchesFilter(bet, filter));
	}

	@Test
	public void testbetMatchesFilter_matchesBetId() {
		when(spyFilter.getBetId()).thenReturn("regularBet-0").thenReturn("regularBet-0");
		when(mockBet.getBetId()).thenReturn("regularBet-0");
		assertTrue(testBetManager.betMatchesFilter(mockBet, spyFilter));
	}

	@Test
	public void testbetMatchesFilter_doesntMatchBetId() {
		when(spyFilter.getBetId()).thenReturn("regularBet-0").thenReturn("regularBet-0");
		when(mockBet.getBetId()).thenReturn("regularBet-1");
		assertFalse(testBetManager.betMatchesFilter(mockBet, spyFilter));
	}

	@Test
	public void testbetMatchesFilter_matchesUserId() {
		when(spyFilter.getUserId()).thenReturn("rb").thenReturn("rb");
		when(mockBet.getUserId()).thenReturn("rb");
		assertTrue(testBetManager.betMatchesFilter(mockBet, spyFilter));
	}

	@Test
	public void testbetMatchesFilter_doesntMatchUserId() {
		when(spyFilter.getUserId()).thenReturn("rb").thenReturn("rb");
		when(mockBet.getUserId()).thenReturn("rv");
		assertFalse(testBetManager.betMatchesFilter(mockBet, spyFilter));
	}

	@Test
	public void testbetMatchesFilter_matchesMinTimeStamp() {
		when(spyFilter.getMinTimeStamp()).thenReturn(currentTimeStamp - 1000).thenReturn(currentTimeStamp - 1000);
		when(mockBet.getTimeStamp()).thenReturn(currentTimeStamp - 100);
		assertTrue(testBetManager.betMatchesFilter(mockBet, spyFilter));
	}

	@Test
	public void testbetMatchesFilter_doesntMatchMinTimeStamp() {
		when(spyFilter.getMinTimeStamp()).thenReturn(currentTimeStamp - 1000).thenReturn(currentTimeStamp - 1000);
		when(mockBet.getTimeStamp()).thenReturn(currentTimeStamp - 2000);
		assertFalse(testBetManager.betMatchesFilter(mockBet, spyFilter));
	}

	@Test
	public void testbetMatchesFilter_matchesMaxTimeStamp() {
		when(spyFilter.getMaxTimeStamp()).thenReturn(currentTimeStamp).thenReturn(currentTimeStamp);
		when(mockBet.getTimeStamp()).thenReturn(currentTimeStamp - 100);
		assertTrue(testBetManager.betMatchesFilter(mockBet, spyFilter));
	}

	@Test
	public void testbetMatchesFilter_doesntMatchMaxTimeStamp() {
		when(spyFilter.getMaxTimeStamp()).thenReturn(currentTimeStamp).thenReturn(currentTimeStamp - 100);
		when(mockBet.getTimeStamp()).thenReturn(currentTimeStamp);
		assertFalse(testBetManager.betMatchesFilter(mockBet, spyFilter));
	}

	@Test
	public void testbetMatchesFilter_matchesMinAmount() {
		when(spyFilter.getMinAmount()).thenReturn(0L).thenReturn(0L);
		when(mockBet.getAmount()).thenReturn(100L);
		assertTrue(testBetManager.betMatchesFilter(mockBet, spyFilter));
	}

	@Test
	public void testbetMatchesFilter_doesntMatchMinAmount() {
		when(spyFilter.getMinAmount()).thenReturn(100L).thenReturn(100L);
		when(mockBet.getAmount()).thenReturn(1L);
		assertFalse(testBetManager.betMatchesFilter(mockBet, spyFilter));
	}

	@Test
	public void testbetMatchesFilter_matchesMaxAmount() {
		when(spyFilter.getMaxAmount()).thenReturn(200L).thenReturn(200L);
		when(mockBet.getAmount()).thenReturn(100L);
		assertTrue(testBetManager.betMatchesFilter(mockBet, spyFilter));
	}

	@Test
	public void testbetMatchesFilter_doesntMatchMaxAmount() {
		when(spyFilter.getMaxAmount()).thenReturn(200L).thenReturn(200L);
		when(mockBet.getAmount()).thenReturn(300L);
		assertFalse(testBetManager.betMatchesFilter(mockBet, spyFilter));
	}

	@Test
	public void testbetMatchesFilter_matchesStatus() {
		when(spyFilter.getStatus()).thenReturn(ACTIVE).thenReturn(ACTIVE);
		when(mockBet.getStatus()).thenReturn(ACTIVE);
		assertTrue(testBetManager.betMatchesFilter(mockBet, spyFilter));
	}

	@Test
	public void testbetMatchesFilter_doesntMatchStatus() {
		when(spyFilter.getStatus()).thenReturn(ACTIVE).thenReturn(ACTIVE);
		when(mockBet.getStatus()).thenReturn(CANCELLED);
		assertFalse(testBetManager.betMatchesFilter(mockBet, spyFilter));
	}

	@Test
	public void testCancelBet_active() {
		testBetManager.cancelBet(bet);
		assertEquals(testBetManager.findBetById(bet.getBetId()).getStatus(), CANCELLED);
	}

	@Test
	public void testFindBetById() {
		Bet betForSearch = new Bet("searchBet-0", "sb", currentTimeStamp, 1, ACTIVE);
		Bet foundBet = testBetManager.findBetById(betForSearch.getBetId());
		assertEquals(foundBet.getStatus(), null);
		assertEquals(foundBet.getBetId(), betForSearch.getBetId());
		testBetManager.addBet(betForSearch);
		foundBet = testBetManager.findBetById(betForSearch.getBetId());
		assertEquals(foundBet, betForSearch);
	}
}