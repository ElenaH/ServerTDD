package com.playtech.intern2017.server.connection;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.playtech.intern2017.server.request.handlers.BetManagerTest;

@RunWith(Suite.class)
@SuiteClasses({
	BetManagerTest.class
})
public class AllTests {

}
