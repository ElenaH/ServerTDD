package com.playtech.intern2017.server.request.handlers;

import com.playtech.intern2017.protocol.data.*;
import com.playtech.intern2017.protocol.messages.*;
import com.playtech.intern2017.protocol.messages.add.*;
import com.playtech.intern2017.protocol.messages.query.Bet;
import com.playtech.intern2017.server.connection.ResponseStatusMessage;

public class AddBetRequestHandler extends RequestHandler {
	private BetManager betManager;

	public AddBetRequestHandler(BetManager betManager) {		
		this.betManager = betManager;
	}

	@Override
	public Response handleRequest(Request re) {
		AddBetRequest request = (AddBetRequest) re;
		ResponseBuilder builder = new AddBetResponseBuilder(request);
		Bet bet = new Bet(request.getBetId(), request.getUserId(), request.getTimeStamp(), request.getAmount(),
				BetStatus.ACTIVE);
		lock.lock();
		try {
			builder = validateBet(bet, builder);
			if (isRequestSuccessful(builder)) {
				betManager.addBet(bet);
			}
		} finally {
			lock.unlock();
		}
		Response response = (Response) builder.build();
		return response;
	}
	
	protected ResponseBuilder validateBet(Bet bet, ResponseBuilder builder) {
		final long RG_LIMIT = 10000;
		long total;
		if (!betManager.isUserIdValid(bet)) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_SYSTEM);
			builder.setStatusMessage(ResponseStatusMessage.USER_ID_INVALID);
		} else if (!betManager.isBetIdValid(bet)) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_SYSTEM);
			builder.setStatusMessage(ResponseStatusMessage.BET_ID_INVALID);
		} else if (betManager.isAmountNegative(bet)) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_SYSTEM);
			builder.setStatusMessage(ResponseStatusMessage.AMOUNT_NEGATIVE);
		} else if (!betManager.isTimeStampAfterStart(bet)) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_SYSTEM);
			builder.setStatusMessage(ResponseStatusMessage.TIMESTAMP_BEFORE_START);
		} else if (!betManager.isTimeStampBeforeEnd(bet)) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_SYSTEM);
			builder.setStatusMessage(ResponseStatusMessage.TIMESTAMP_IN_FUTURE);
		} else if (!betManager.isUnique(bet)) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_NONUNIQUE_MESSAGE);
			builder.setStatusMessage(ResponseStatusMessage.BET_ID_NONUNIQUE + bet.getBetId());
		} else if ((total = betManager.rgLimitExceeded(bet)) > RG_LIMIT) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_LIMIT_VIOLATION_MESSAGE);
			builder.setStatusMessage(ResponseStatusMessage.RG_LIMIT_EXCEEDED + total);
		} else {
			builder.setStatusCode(ResponseStatus.STATUS_SUCCESS);
		}
		return builder;
	}
}