package com.playtech.intern2017.server.connection;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {
	
	public static boolean datesEqual(long time1, long time2) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		format.setTimeZone(TimeZone.getTimeZone("UTC"));
		String date1String = format.format(new Date(time1));
		String date2String = format.format(new Date(time2));
		return date1String.equals(date2String);
	}
}