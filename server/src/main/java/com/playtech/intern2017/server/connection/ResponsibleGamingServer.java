package com.playtech.intern2017.server.connection;

import static com.playtech.intern2017.protocol.converter.ProtocolConversionUtils.*;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.*;
import org.apache.log4j.Logger;

import com.playtech.intern2017.protocol.messages.*;
import com.playtech.intern2017.protocol.messages.add.AddBetRequest;
import com.playtech.intern2017.protocol.messages.cancel.CancelBetRequest;
import com.playtech.intern2017.protocol.messages.query.QueryBetsRequest;
import com.playtech.intern2017.server.request.handlers.AddBetRequestHandler;
import com.playtech.intern2017.server.request.handlers.BetManager;
import com.playtech.intern2017.server.request.handlers.CancelBetRequestHandler;
import com.playtech.intern2017.server.request.handlers.QueryBetsRequestHandler;
import com.playtech.intern2017.server.request.handlers.RequestHandler;

public class ResponsibleGamingServer implements Runnable {
	private final int SERVER_SOCKET_PORT;
	private final int THREAD_POOL_LIMIT = 4;
	private boolean keepRunning = true;
	private final static Logger SERVER_LOGGER = Logger.getLogger(ResponsibleGamingServer.class);
	public BetManager betManager;
	
	public ResponsibleGamingServer(int serverSocketPort) {
		this.SERVER_SOCKET_PORT = serverSocketPort;		
		this.betManager = new BetManager();
		this.keepRunning = true;
	}

	public void start() {
		new Thread(this).start();
	}

	public void run() {
		SERVER_LOGGER.info("ResponsibleGamingServer is running.");
		ExecutorService poolForGamingServers = Executors.newFixedThreadPool(THREAD_POOL_LIMIT);
		try (ServerSocket listener = new ServerSocket(this.SERVER_SOCKET_PORT)) {
			while (keepRunning) {
				poolForGamingServers.execute(new ServerThread(listener.accept(), betManager));
			}
		} catch (Exception e) {
			SERVER_LOGGER.error("ResponsibleGamingServer runtime errors: " + e);
		}
	}

	public void close() {
		this.keepRunning = false;
	}
}

class ServerThread extends Thread {
	private Socket socket;
	private final static Logger SERVER_LOGGER = Logger.getLogger(ServerThread.class);
	private BetManager betManager;
	
	public ServerThread(Socket socket, BetManager betManager) {
		this.socket = socket;
		this.betManager= betManager;
	}

	@Override
	public void run() {
		SERVER_LOGGER.info(this + ": Server Started and listening to the port " + socket.getPort());
		try (OutputStream output = socket.getOutputStream();
				BufferedReader input = new BufferedReader(
						new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8))) {
			String request;			
			while ((request = input.readLine()) != null) {
				respondToRequest(output, request);				
			}
		} catch (Exception e) {
			SERVER_LOGGER.error(this + ": Unable to send response", e);
		}
	}

	private void respondToRequest(OutputStream output, String requestJson) throws Exception {			
		Response response = makeResponse (unmarshall(requestJson));
		output.write((marshall(response) + "\n").getBytes(StandardCharsets.UTF_8));
		output.flush();		
	}		
	
	private Response makeResponse(Request request){
		RequestHandler requestHandler = identifyAndMakeHandler(betManager, request);		
		Response response = requestHandler.handleRequest(request);		
		return response;		
	}
	
	private RequestHandler identifyAndMakeHandler(BetManager betManager, Request request)throws IllegalArgumentException{
		if (request instanceof AddBetRequest) {
			return new AddBetRequestHandler(betManager);
		} else if (request instanceof CancelBetRequest) {
			return new CancelBetRequestHandler(betManager);
		} else if (request instanceof QueryBetsRequest) {
			return new QueryBetsRequestHandler(betManager);
		} else {
			throw new IllegalArgumentException();
		}
	}	
}