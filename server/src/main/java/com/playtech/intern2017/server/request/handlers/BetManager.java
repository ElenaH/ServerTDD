package com.playtech.intern2017.server.request.handlers;

import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import com.playtech.intern2017.protocol.data.BetStatus;
import com.playtech.intern2017.protocol.messages.query.Bet;
import com.playtech.intern2017.protocol.messages.query.BetFilter;

import static com.playtech.intern2017.server.connection.DateUtil.*;

public class BetManager {
	private static Collection<Bet> allBets = new CopyOnWriteArrayList<Bet>();

	public Collection<Bet> getAllBets() {
		return allBets;
	}

	
	public boolean isBetIdValid(Bet bet) {
		if ((bet.getBetId() == null) || (bet.getBetId().trim().isEmpty())) {
			return false;
		} else {
			return true;
		}
	}

	public boolean isUserIdValid(Bet bet) {
		if (bet.getUserId() == null) {
			return false;
		} else if (bet.getUserId().trim().isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	public boolean isTimeStampAfterStart(Bet bet) {
		final long START_TIMESTAMP = (long) 1488326400 * 1000;
		if (bet.getTimeStamp() < START_TIMESTAMP) {
			return false;
		} else {
			return true;
		}
	}

	public boolean isTimeStampBeforeEnd(Bet bet) {
		long currentTimeStamp = new Timestamp(System.currentTimeMillis()).getTime();
		if (bet.getTimeStamp() > currentTimeStamp) {
			return false;
		} else {
			return true;
		}
	}

	public boolean isAmountNegative(Bet bet) {
		if (bet.getAmount() < 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isAlreadyCancelled(Bet bet) {
		if (bet.getStatus().equals(BetStatus.CANCELLED)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean doesntExist(Bet bet) {
		if (bet.getStatus() == null) {
			return true;
		} else {
			return false;
		}
	}

	public void addBet(Bet bet) {
		allBets.add(bet);

	}

	public boolean isUnique(Bet myBet) {
		if (doesntExist(findBetById(myBet.getBetId()))) {
			return true;
		} else {
			return false;
		}
	}

	public boolean betMatchesFilter(Bet bet, BetFilter filter) {
		if (filter == null) {
			return true;
		}
		if ((filter.getBetId() != null) && (!filter.getBetId().equals(bet.getBetId()))) {
			return false;
		}
		if ((filter.getUserId() != null) && (!filter.getUserId().equals(bet.getUserId()))) {
			return false;
		}
		if ((filter.getMinTimeStamp() != null) && (filter.getMinTimeStamp() > bet.getTimeStamp())) {
			return false;
		}
		if ((filter.getMaxTimeStamp() != null) && (filter.getMaxTimeStamp() < bet.getTimeStamp())) {
			return false;
		}
		if ((filter.getMinAmount() != null) && (filter.getMinAmount() > bet.getAmount())) {
			return false;
		}
		if ((filter.getMaxAmount() != null) && (filter.getMaxAmount() < bet.getAmount())) {
			return false;
		}
		if ((filter.getStatus() != null) && (!filter.getStatus().equals(bet.getStatus()))) {
			return false;
		}
		return true;
	}

	public Bet findBetById(String betId) {
		for (Bet bet : allBets) {
			if (bet.getBetId().equals(betId)) {
				return bet;
			}
		}
		return new Bet(betId, null, 0, 0, null);
	}
	
	public void cancelBet(Bet bet){
		allBets.remove(bet);
		allBets.add(new Bet(bet.getBetId(), bet.getUserId(), bet.getTimeStamp(), bet.getAmount(), BetStatus.CANCELLED));
	}

//added without tests
	public TreeSet<Bet> getFilteredBets(BetFilter filter) {
		class BetIdComparator implements Comparator<Bet> {
			public int compare(Bet bet1, Bet bet2) {
				return bet1.getBetId().compareTo(bet2.getBetId());
			}
		}
		TreeSet<Bet> filteredBets = new TreeSet<Bet>(new BetIdComparator());
		for (Bet bet : allBets) {
			if (betMatchesFilter(bet, filter)) {
				filteredBets.add(bet);
			}
		}
		return filteredBets;
	}

//added without tests!!
	public long rgLimitExceeded(Bet newBet) {
		long totalAmount = newBet.getAmount();
		for (Bet bet : allBets) {
			if (bet.getUserId().equals(newBet.getUserId()) && (bet.getStatus().equals(BetStatus.ACTIVE))) {
				if (datesEqual(newBet.getTimeStamp(), bet.getTimeStamp())) {
					totalAmount = totalAmount + bet.getAmount();
				}
			}
		}
		return totalAmount;
	}
}