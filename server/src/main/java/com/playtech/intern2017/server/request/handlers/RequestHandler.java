package com.playtech.intern2017.server.request.handlers;

import java.util.concurrent.locks.*;
import com.playtech.intern2017.protocol.data.*;
import com.playtech.intern2017.protocol.messages.*;

public abstract class RequestHandler {
	static final Lock lock = new ReentrantLock();
	
	public abstract Response handleRequest(Request request);

	
	public boolean isRequestSuccessful(ResponseBuilder builder) {
		return (builder.getStatusCode() == ResponseStatus.STATUS_SUCCESS);
	}
}