package com.playtech.intern2017.server.request.handlers;

import com.playtech.intern2017.protocol.data.ResponseStatus;
import com.playtech.intern2017.protocol.messages.*;
import com.playtech.intern2017.protocol.messages.query.*;

public class QueryBetsRequestHandler extends RequestHandler {
	private BetManager betManager;
	
	public QueryBetsRequestHandler(BetManager betManager) {
		this.betManager = betManager;
	}
	
	@Override
	public Response handleRequest(Request re) {
		QueryBetsRequest request =(QueryBetsRequest) re;
		QueryBetsResponseBuilder newResponseBuilder = new QueryBetsResponseBuilder(request);
		BetFilter newFilter = request.getFilter();
		newResponseBuilder.setBets(betManager.getFilteredBets(newFilter));
		newResponseBuilder.setStatusCode(ResponseStatus.STATUS_SUCCESS);
		Response response = (Response) newResponseBuilder.build();
		return response;
	}
}
