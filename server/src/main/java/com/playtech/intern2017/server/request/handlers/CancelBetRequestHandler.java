package com.playtech.intern2017.server.request.handlers;

import com.playtech.intern2017.protocol.data.ResponseStatus;
import com.playtech.intern2017.protocol.messages.*;
import com.playtech.intern2017.protocol.messages.cancel.*;
import com.playtech.intern2017.protocol.messages.query.Bet;
import com.playtech.intern2017.server.connection.ResponseStatusMessage;

public class CancelBetRequestHandler extends RequestHandler {
	private BetManager betManager;
	
	public CancelBetRequestHandler(BetManager betManager) {
		this.betManager = betManager;
	}

	@Override
	public Response handleRequest(Request re) {
		CancelBetRequest request =(CancelBetRequest) re;
		ResponseBuilder builder = new CancelBetResponseBuilder(request);
		String betId = request.getBetId();
		lock.lock();
		try {
			Bet bet = betManager.findBetById(betId);
			builder = validateCancelation(bet, builder);
			if (isRequestSuccessful(builder)) {
				betManager.cancelBet(bet);
			}
		} finally {
			lock.unlock();
		}
		Response response = (Response) builder.build();
		return response;
	}
	
	private ResponseBuilder validateCancelation(Bet bet, ResponseBuilder builder) {
		if (betManager.doesntExist(bet)) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_NOT_FOUND);
			builder.setStatusMessage(ResponseStatusMessage.NOT_FOUND + bet.getBetId());
		} else if (betManager.isAlreadyCancelled(bet)) {
			builder.setStatusCode(ResponseStatus.STATUS_ERR_INVALID);
			builder.setStatusMessage(ResponseStatusMessage.ALREADY_CANCELLED + bet.getBetId());
		} else {
			builder.setStatusCode(ResponseStatus.STATUS_SUCCESS);
		}
		return builder;
	}	
}