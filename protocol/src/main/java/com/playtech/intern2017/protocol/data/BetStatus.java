package com.playtech.intern2017.protocol.data;

public enum BetStatus {

	ACTIVE, CANCELLED;

}