package com.playtech.intern2017.protocol.messages.add;

import com.playtech.intern2017.protocol.messages.ResponseBuilder;

public class AddBetResponseBuilder extends ResponseBuilder {

	public AddBetResponseBuilder() {}
	
	public AddBetResponseBuilder(AddBetRequest request) {
		super(request);
	}
	
	public AddBetResponse build() {
		return new AddBetResponse(getMessageId(), getStatusCode(), getStatusMessage());
	}

}
